class AgedBrie {
    private sellIn: number;
    private quality: number;
    constructor(sellIn: number, quality: number) {
        this.sellIn = sellIn;
        this.quality = quality;
    }
    passOneDay() {
        this.sellIn--
        if(this.isQualityLessThanFifth()){
            this.quality = this.isPastDue()?this.qualityCastUp(2):this.qualityCastUp(1)
        }
    }

    private qualityCastUp(cutQuality:number) {
        return this.quality + cutQuality;
    }

    private isPastDue() {
        return this.sellIn < 0;
    }

    private isQualityLessThanFifth() {
        return this.quality < 50 ;
    }

    getSellIn() {
        return this.sellIn;
    }

    getQuality() {
        return this.quality;
    }
}
export default AgedBrie
