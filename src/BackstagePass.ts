class BackstagePass {
    private sellIn: number;
    private quality: number;
    constructor(sellIn: number, quality: number) {
        this.sellIn = sellIn;
        this.quality = quality;
    }
    passOneDay() {
        this.sellIn--
        if(this.isQualityLessThanFifth()){
            if(this.sellIn > 10){
                this.quality = this.qualityCastUp(1)
            }else if(this.sellIn <=10 && this.sellIn > 5){
                this.quality = this.qualityCastUp(2)
            }else if(this.sellIn <= 5 && this.sellIn >= 0){
                this.quality = this.qualityCastUp(3)
            }else if(this.sellIn < 0){
                this.quality = 0
            }
        }
        if(!this.isQualityLessThanFifth()){
            this.quality = 50
        }
    }

    private qualityCastUp(cutQuality:number) {
        return this.quality + cutQuality;
    }

    private isQualityLessThanFifth() {
        return this.quality < 50 ;
    }

    getSellIn() {
        return this.sellIn;
    }

    getQuality() {
        return this.quality;
    }
}
export default BackstagePass
