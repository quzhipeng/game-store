class Sulfuras {
    private sellIn: number;
    private readonly quality: number;
    constructor(sellIn: number, quality: number) {
        this.sellIn = sellIn;
        this.quality = quality;
    }
    passOneDay() {
        this.sellIn--
    }
    getSellIn() {
        return this.sellIn;
    }

    getQuality() {
        return this.quality;
    }
}
export default Sulfuras
