class NormalGoods {
    private sellIn: number;
    private quality: number;
    constructor(sellIn: number, quality: number) {
        this.sellIn = sellIn;
        this.quality = quality;
    }
    passOneDay() {
        this.sellIn--
        if(this.isQualityEqualZero()){
            this.quality = this.isPastDue()?this.qualityCutDown(2):this.qualityCutDown(1)
        }
    }

    private qualityCutDown(cutQuality:number) {
        return this.quality - cutQuality;
    }

    private isPastDue() {
        return this.sellIn < 0;
    }

    private isQualityEqualZero() {
        return this.quality > 0;
    }

    getSellIn() {
        return this.sellIn;
    }

    getQuality() {
        return this.quality;
    }
}
export default NormalGoods
