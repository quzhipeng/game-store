import AgedBrie from "../src/AgedBrie";

describe('AgedBrie', () => {
    test('should get sellIn is 9 and quality is 21 when pass one day given AgedBrie sellIn 10 and quality 20',()=>{
        //given
        const agedBrie: AgedBrie = new AgedBrie(10,20)

        //when
        agedBrie.passOneDay();

        //then
        let sellIn = agedBrie.getSellIn();
        let quality = agedBrie.getQuality();
        expect(sellIn).toBe(9);
        expect(quality).toBe(21)
    });
    test('should get sellIn is -1 and quality is 22 when pass one day given AgedBrie sellIn 0 and quality 20',()=>{
        //given
        const agedBrie: AgedBrie = new AgedBrie(0,20)

        //when
        agedBrie.passOneDay();

        //then
        let sellIn = agedBrie.getSellIn();
        let quality = agedBrie.getQuality();
        expect(sellIn).toBe(-1);
        expect(quality).toBe(22)
    });

    test('should get sellIn is -2 and quality is 22 when pass one day given AgedBrie sellIn -1 and quality 20',()=>{
        //given
        const agedBrie: AgedBrie = new AgedBrie(-1,20)

        //when
        agedBrie.passOneDay();

        //then
        let sellIn = agedBrie.getSellIn();
        let quality = agedBrie.getQuality();
        expect(sellIn).toBe(-2);
        expect(quality).toBe(22)
    });

    test('should get sellIn is 9 and quality is 50 when pass one day given AgedBrie sellIn 10 and quality 50',()=>{
        //given
        const agedBrie: AgedBrie = new AgedBrie(10,50)

        //when
        agedBrie.passOneDay();

        //then
        let sellIn = agedBrie.getSellIn();
        let quality = agedBrie.getQuality();
        expect(sellIn).toBe(9);
        expect(quality).toBe(50)
    });
});
