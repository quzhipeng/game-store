import Sulfuras from "../src/Sulfuras";

describe('Sulfuras', () => {
    test('should get sellIn is 0 and quality is 20 when pass one day given Sulfuras sellIn 1 and quality 20',()=>{
        //given
        const sulfuras: Sulfuras = new Sulfuras(1,20)

        //when
        sulfuras.passOneDay();

        //then
        let sellIn = sulfuras.getSellIn();
        let quality = sulfuras.getQuality();
        expect(sellIn).toBe(0);
        expect(quality).toBe(20)
    });
    test('should get sellIn is -1 and quality is 50 when pass one day given Sulfuras sellIn 0 and quality 50',()=>{
        //given
        const sulfuras: Sulfuras = new Sulfuras(0,50)

        //when
        sulfuras.passOneDay();

        //then
        let sellIn = sulfuras.getSellIn();
        let quality = sulfuras.getQuality();
        expect(sellIn).toBe(-1);
        expect(quality).toBe(50)
    });
});
