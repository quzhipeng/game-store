import NormalGoods from "../src/NormalGoods";

describe('normalGoods', () => {
    test('should get sellIn is 9 and quality is 19 when pass one day given normal goods sellIn 10 and quality 20',()=>{
        //given
        const normalGoods: NormalGoods = new NormalGoods(10,20)

        //when
        normalGoods.passOneDay();

        //then
        let sellIn = normalGoods.getSellIn();
        let quality = normalGoods.getQuality();
        expect(sellIn).toBe(9);
        expect(quality).toBe(19)
    });

    test('should get sellIn is -1 and quality is 18 when pass one day given normal goods sellIn 0 and quality 20',()=>{
        //given
        const normalGoods: NormalGoods = new NormalGoods(0,20)

        //when
        normalGoods.passOneDay();

        //then
        let sellIn = normalGoods.getSellIn();
        let quality = normalGoods.getQuality();
        expect(sellIn).toBe(-1);
        expect(quality).toBe(18)
    });

    test('should get sellIn is -2 and quality is 18 when pass one day given normal goods sellIn -1 and quality 20',()=>{
        //given
        const normalGoods: NormalGoods = new NormalGoods(-1,20)

        //when
        normalGoods.passOneDay();

        //then
        let sellIn = normalGoods.getSellIn();
        let quality = normalGoods.getQuality();
        expect(sellIn).toBe(-2);
        expect(quality).toBe(18)
    });

    test('should get sellIn is 9 and quality is 0 when pass one day given normal goods sellIn 10 and quality 0',()=>{
        //given
        const normalGoods: NormalGoods = new NormalGoods(10,0)

        //when
        normalGoods.passOneDay();

        //then
        let sellIn = normalGoods.getSellIn();
        let quality = normalGoods.getQuality();
        expect(sellIn).toBe(9);
        expect(quality).toBe(0)
    });
});
