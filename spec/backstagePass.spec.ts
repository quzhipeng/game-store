import BackstagePass from "../src/BackstagePass";

describe('BackstagePass', () => {
    test('should get sellIn is 14 and quality is 21 when pass one day given BackstagePass sellIn 15 and quality 21',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(15,20)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(14);
        expect(quality).toBe(21)
    });
    test('should get sellIn is 9 and quality is 22 when pass one day given BackstagePass sellIn 10 and quality 20',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(10,20)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(9);
        expect(quality).toBe(22)
    });

    test('should get sellIn is 9 and quality is 50 when pass one day given BackstagePass sellIn 10 and quality 49',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(10,49)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(9);
        expect(quality).toBe(50)
    });

    test('should get sellIn is 8 and quality is 22 when pass one day given BackstagePass sellIn 9 and quality 20',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(9,20)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(8);
        expect(quality).toBe(22)
    });

    test('should get sellIn is 4 and quality is 23 when pass one day given BackstagePass sellIn 5 and quality 20',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(5,20)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(4);
        expect(quality).toBe(23)
    });

    test('should get sellIn is 3 and quality is 50 when pass one day given BackstagePass sellIn 4 and quality 48',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(4,48)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(3);
        expect(quality).toBe(50)
    });

    test('should get sellIn is 0 and quality is 23 when pass one day given BackstagePass sellIn 1 and quality 20',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(1,20)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(0);
        expect(quality).toBe(23)
    });

    test('should get sellIn is -1 and quality is 0 when pass one day given BackstagePass sellIn 0 and quality 20',()=>{
        //given
        const backstagePass: BackstagePass = new BackstagePass(0,20)

        //when
        backstagePass.passOneDay();

        //then
        let sellIn = backstagePass.getSellIn();
        let quality = backstagePass.getQuality();
        expect(sellIn).toBe(-1);
        expect(quality).toBe(0)
    });
});
